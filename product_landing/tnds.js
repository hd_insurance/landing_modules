import React, { useEffect, useState, createRef, useRef, forwardRef } from "react";
import { animateScroll } from "react-scroll";
import DynamicRender from "../../components/render";
import { confirmAlert } from "react-confirm-alert";
import api from "../../services/Network.js";
import moment, { now } from "moment";
import cogoToast from "cogo-toast";
import currencyFormatter from "currency-formatter";
import util from "../../components/render/util";


const Main = forwardRef((props, ref) => {
  const footerRef = useRef();

  const [loading, setLoading] = useState(true);

  const [define, setDefine] = useState({});
  const [listmaped, setMaped] = useState({});
 

  useEffect(() => {
      const { obj_config, map } = util.rootObjectComponent(props.page_layout);
      setMaped(map);
      setDefine(obj_config);
      setTimeout(() => {
        setLoading(false);
      }, 200);
  }, []);

  useEffect(() => {
    if (!util.isEmptyObj(listmaped)) { 
      if(listmaped["page_header"] || listmaped["page_benefit_intro"]){
        if(listmaped["page_header"]){
          listmaped["page_header"].onScrollForm = () => {
            listmaped['page_form'].ref.current.scrollIntoView({behavior: 'smooth'});
          };
        }
        if(listmaped["page_benefit_intro"]){
          listmaped["page_benefit_intro"].onScrollForm = () => {
            listmaped['page_form'].ref.current.scrollIntoView({behavior: 'smooth'});
          }
        }
        
      }
    }
  },[listmaped]);



  return (
    <div>
       {loading ? (
        <div >
          <div>
            
          </div>
        </div>
      ) : (<DynamicRender
              layout={define}
              productConfig={props.product_config}
            />)}
    </div>
  );
})


export default Main;
