import React, { useEffect, useState, createRef, useRef, forwardRef } from "react";
import { animateScroll } from "react-scroll";
import DynamicRender from "../../components/render";
import { confirmAlert } from "react-confirm-alert";
import { Modal, Button } from "react-bootstrap";
import api from "../../services/Network.js";
import moment, { now } from "moment";
import cogoToast from "cogo-toast";
import currencyFormatter from "currency-formatter";
import util from "../../components/render/util";
import ClaimSDK from "../../sdk/src/sdk_modules/eclaim";
import Sheet from 'react-modal-sheet';

import ClaimResult from "./claim_result/style1.js"


import Modal1 from "./modal/modal1.js" //xac nhan bt
import Modal2 from "./modal/modal2.js" //tu choi bt
import Modal3 from "./modal/modal3.js" //bo sung giay to



const Main = forwardRef((props, ref) => {

  const footerRef = useRef();

  const [loading, setLoading] = useState(true);
  const [searchloading, setSearchLoading] = useState(false);

  const [define, setDefine] = useState({});
  const [listmaped, setMaped] = useState({});
  const [show, setShow] = useState(false);

  const [list_product_all, setListProductAll] = useState({});
  const [list_product, setListProduct] = useState([]);
  const [list_cateogry, setListCategory] = useState([]);
  const [current_cateogry, setCurrentCategory] = useState(null);
  const [current_product, setCurrentProduct] = useState(null);
  const [current_product_name, setCurrentProductName] = useState(null);
  const [modalState, setModalState] = useState(null);
  const [openCaptcha, setOpenCaptcha] = useState(false);
  const [searchresult, setResult] = useState({show: false});
  
  const [claimCode, setClaimCode] = useState(null);
  const [claimStatus, setClaimStatus] = useState(null);

  const [modalLayout, setModalLayout] = useState(null);
  const [current_tab, setCurrentTab] = useState("0");

  //form tra cuu
  // const [field1, setField1] = useState("123456788");
  // const [field2, setField2] = useState("BOSUNG");

    //form tra cuu
  // const [field1, setField1] = useState("1234567455");
  // const [field2, setField2] = useState("XACNHAN");

    //form tra cuu
  // const [field1, setField1] = useState("234567654");
  // const [field2, setField2] = useState("TUCHOI");

  const [field1, setField1] = useState("");
  const [field2, setField2] = useState("");



  //modal
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
      const { obj_config, map } = util.rootObjectComponent(props.pageConfig.layout_component);
      setMaped(map);
      setDefine(obj_config);
      setTimeout(() => {
        setLoading(false);
      }, 200);

  }, []);
  useEffect(() => {
     if(props.router_query.field1 && props.router_query.field2){
        setCurrentTab("1")
      }
  }, [props.router_query]);

  useEffect(() => {
    if (!util.isEmptyObj(listmaped)) { 
      getProductDefine()
       // console.log("call api tra cuu --- ", listmaped.form_tracuu)
    }
  },[listmaped]);


  const getProductDefine = async ()=>{
    // lay danh sach tat ca san pham
    try{
      const res = await api.get("/api/eclaim/product-list/define")
      if(res.data[0]){
        var obj_out = {}
        var list_category = []
        res.data[0].forEach((product, index)=>{
          if(obj_out[product.GROUP_INSUR]){
            obj_out[product.GROUP_INSUR].push({label: product.PRODUCT_NAME, group_name:product.GROUP_NAME,  value: product.PRODUCT_CODE})
          }else{
            obj_out[product.GROUP_INSUR] = []
            obj_out[product.GROUP_INSUR].push({label: product.PRODUCT_NAME, group_name:product.GROUP_NAME, value: product.PRODUCT_CODE})
          }
        })
        Object.keys(obj_out).forEach((group, index)=>{
          list_category.push({label: obj_out[group][0].group_name, value: group})
        })
        setListProductAll(obj_out)
        setListCategory(list_category)

        if(props.router_query.field1 && props.router_query.field2){
          setField1(props.router_query.field1)
          setField2(props.router_query.field2)
          setCurrentTab("1")
          searching(props.router_query.field1, props.router_query.field2)
        }


      }
    }catch(err){
      console.log("err", err)
    }
    
  }




  const onCreateClaimClick = ()=>{
    if(current_product && current_cateogry){
     document.body.style.overflow = 'hidden';
     setShow(true)
    }else{
      cogoToast.error("Vui lòng chọn chương trình bảo hiểm")
    }
    
  }

  const onModalClose = ()=>{
    setShow(false)
     document.body.style.overflow = 'unset';
  }

  const onCategoryChange = (name, value)=>{
      setListProduct(list_product_all[value])
      listmaped.product_insur.setDisableEditor(false)
      if(list_product_all[value][0]){
        setCurrentProductName(list_product_all[value][0].label)
        setCurrentProduct(list_product_all[value][0].value)
      }
      setCurrentCategory(value)
  }
  const onProductChange = (name, value)=>{
    setCurrentProduct(value)
    const searchArray = list_product.filter(function( obj ) {
      return obj.value == value;
    });
    setCurrentProductName(searchArray[0].label)
  }

  const onCallback = async (token, isDisableLoading)=>{

    if(listmaped.form_tracuu.ref.current.handleSubmit()){
      setOpenCaptcha(true)

      if(token){
        if(!isDisableLoading){
          setSearchLoading(true)
        }
          searching(field1, field2)

      }
    }
  }


  const searching = async (f1, f2)=>{
    try{
             const res = await api.post("/api/eclaim/search",{field1: f1, field2: f2})
             listmaped.claim_search_result.setHidden(false)
              setSearchLoading(false)
               if(res.data[0][0]){
                  setClaimStatus(res.data[0][0].STATUS)
                const reslt = {
                  show: true,
                  claim_code: f2,
                  info: res.data[0][0],
                  progress: res.data[4],
                  attached_files: res.data[3]
                }
                const mdState = {
                  require_amount: amountFormat(res.data[0][0].REQUIRED_AMOUNT||0),
                  amount_indemnify: res.data[5],
                  email: res.data[0][0].EMAIL,
                  current_product: res.data[0][0].PRODUCT_CODE,
                  beneciary_info: {
                    NAME: reslt.info.NAME,
                    BENEFICIARY_GENDER: reslt.info.BENEFICIARY_GENDER,
                    BENEFICIARY_NAME: reslt.info.BENEFICIARY_NAME,
                    BENEFICIARY_PHONE: reslt.info.BENEFICIARY_PHONE,
                    BENEFICIARY_IDCARD: reslt.info.BENEFICIARY_IDCARD,
                    BENEFICIARY_TYPE: reslt.info.BENEFICIARY_TYPE,
                    BENEFICIARY_TYPE_NAME: reslt.info.BENEFICIARY_TYPE_NAME,
                  },
                  bank_info: {
                    PAYMENT_TYPE: reslt.info.PAYMENT_TYPE,
                    PAYMENT_NAME: reslt.info.PAYMENT_NAME,
                    ACCOUNT_BANK: reslt.info.ACCOUNT_BANK,
                    BRANCH_BANK: reslt.info.BRANCH_BANK,
                    BANK_ADDRESS: reslt.info.BANK_ADDRESS,
                    ACCOUNT_NAME: reslt.info.ACCOUNT_NAME,
                    ACCOUNT_NO: reslt.info.ACCOUNT_NO,
                    SWIFT_CODE: reslt.info.SWIFT_CODE,
                    BENEFICIARY_ADDRESS: reslt.info.BENEFICIARY_ADDRESS,
                    INTER_SWIFT_CODE: reslt.info.INTER_SWIFT_CODE,
                    INTERMEDIATE_BANK: reslt.info.INTERMEDIATE_BANK
                  },
                  attached_files: res.data[6]
                }
                
                setResult(reslt)
                setModalState(mdState)
                setClaimCode(reslt.info.CLAIM_CODE)
               }else{
                const reslt = {
                  show: false,
                  info: null
                }
                setResult(reslt)
               }
          }catch(e){
              console.log("e ", e)
          }
  }

  const onInputChange = (name, value)=>{
    if(name=="field1"){
      setField1(value)
    }
    if(name=="field2"){
      setField2(value)
    }
  }


  const eventResult = {
    onClickBtnConfirm: ()=>{
      console.log("claimStatus ", claimStatus)
      if(claimStatus == "DYKH"){
        loadLayout("615c2160242c430775e52b36")
      }else if(claimStatus == "TCKH"){
        loadLayout("615c2199242c430775e52b37")
      }else if(claimStatus == "YCBS"){
        loadLayout("615c21b9242c430775e52b38")
      }
      

    }
  }

  const amountFormat = (am)=>{
    return currencyFormatter.format(am, {
            code: l.g("bhsk.currency"),
            precision: 0,
            format: "%v %s",
            symbol: l.g("bhsk.currency"),
          })
  }

  const onCloseClick = (isReload) =>{
    
    setShowModal(false)
    searching(field1, field2)

  }


  const loadLayout = async (layout_id)=>{
    try{
      const response = await api.get(`/api/layout/${layout_id}`)
      if(response.data.layout_component){
        setModalLayout(response.data.layout_component)
        setShowModal(true)
      }
    }catch(e){
      console.error(e)
    }
  }


  return (
    <div>
       {loading ? (
        <div >
          <div>
            
          </div>
        </div>
      ) : (<div><DynamicRender
              layout={define}
              productConfig={props.product_config}
              onCreateClaimClick={onCreateClaimClick}
              resultcomponent={ClaimResult}
              resultProps={{...searchresult, ...eventResult, field2:field2, language: props.pageConfig.layout_component.internal_language}}
              list_product={list_product}
              list_cateogry={list_cateogry}
              onCategoryChange={onCategoryChange}
              onProductChange={onProductChange}
              product={current_product}
              onCaptchaCallback={onCallback}
              onCaptchaClick={onCallback}
              openCaptcha={openCaptcha}
              searchloading={searchloading}
              current_tab={current_tab}
              //form tra cuu
              field1={field1}
              field2={field2}
              onInputChange={onInputChange}
            />


             <Sheet isOpen={show} onClose={() => onModalClose()}>
             <div className="oh-sheet">
                 <div className="sheet-title">{current_product_name}</div>
                 <div className="sheet-close-button" onClick={()=>onModalClose()}><i class="fa fa-times" aria-hidden="true"></i> Close</div>
              </div>

              <Sheet.Container>
                <Sheet.Header/>
                

                <Sheet.Content>
                <div className="form-contain eclaim-f">
                  <ClaimSDK
                    current_category={current_cateogry}
                    current_product={current_product}
                    current_product_name={current_product_name}
                  />
                </div>
                </Sheet.Content>
              </Sheet.Container>
              <Sheet.Backdrop 
                onClick={() => onModalClose()}
              ></Sheet.Backdrop>
            </Sheet>


            <Modal
                show={showModal}
                onHide={() => onCloseClick()}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                enforceFocus={false}
              >
           
            <Modal.Body>
              {claimStatus == "DYKH"&&<Modal1 data={modalState} claimCode={claimCode} onCloseClick={onCloseClick} layout={modalLayout}/>}
              {claimStatus == "TCKH"&&<Modal2 data={modalState} claimCode={claimCode} onCloseClick={onCloseClick} layout={modalLayout}/>}
              {claimStatus == "YCBS"&&<Modal3 data={modalState} claimCode={claimCode} onCloseClick={onCloseClick} layout={modalLayout}/>}

            </Modal.Body>

            
            
          </Modal>





            </div>)}
    </div>
  );
})


export default Main;
