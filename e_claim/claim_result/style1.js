import React, { useEffect, useState, createRef, useRef, forwardRef } from "react";
import { Accordion, Tab, Nav, Row, Col } from "react-bootstrap";
import {UploadFrame} from "hdi-uikit";
import currencyFormatter from "currency-formatter";
import ScrollContainer from 'react-indiana-drag-scroll'
var mobile = require('is-mobile');
var _ = require("lodash");


const current_language = {
  key_YCBS: "Bổ sung giấy tờ",
  key_DYKH: "Xác nhận phương án bồi thường",
  key_TCKH: "Xác nhận từ chối bồi thường",
  product_name: "Chương trình BH",
  request_name: "Người yêu cầu",
  status: "Trạng thái",
  issuer: "Người được BH",
  request_date: "Ngày yêu cầu",
  gnc_no: "Số giấy chứng nhận",
  amount_claim: "Số tiền yêu cầu",
  time_iss: "Thời hạn bảo hiểm",
  pay_method: "Hình thức nhận bồi thường",
  cer_no:"Hồ sơ bồi thường",
  request: "Yêu cầu",
  info: "Thông tin",
  document: "Hồ sơ",
  progress: "Quá trình",
  cls: "Thu gọn",
  claim_detail: "Chi tiết bồi thường",
  no_result: "Không có kết quả",
  noresult_note: "Không tìm thấy kết quả cho thông tin tìm kiếm, vui lòng kiểm tra lại thông tin hoặc liên hệ bộ phận cskh."
}



const ClaimResult = forwardRef((props, ref) => {

  const [language, setLanguage] = useState(props.language?props.language:current_language);
  const [loading, setLoading] = useState(false);
  const [expand, setExpand] = useState(false);
  const [isMobile, setIsMobile] = useState(mobile);
  const [isShowPop, setShowPop] = useState(false);
  const [defaultActive, setDefaultActive] = useState("t01");
  const [docs, setDocs] = useState([]);
  const [showArrow, setShowArrow] = useState(false);
  const onScreen = useWindowSize();
  var scroll_ref = useRef()
  useEffect(() => {
    // Update the document title using the browser API
    setIsMobile(mobile())
  }, [onScreen]);

  useEffect(() => {
        var layout_docs = []
        var grouped = _.mapValues(_.groupBy(props.attached_files, "GROUP_ID"), (clist) =>
          clist.map((car) => _.omit(car, "GROUP_ID"))
        );
        for (const [key, value] of Object.entries(grouped)) {
          let ite = {
            "group_name": value[0].GROUP_NAME,
            "GROUP_ID": key,
            "files":[]
          }
          value.forEach((frame, index) => {
            ite.files.push({
              "id": index+"-"+key.toString(),
              "label": frame.FILE_NAME,
              "key": frame.FILE_KEY,
              "description": frame.GROUP_DESC,
              "required": true,
              "max": 10,
              "list": frame.FILE_ID?[{url: `${process.env.NEXT_PUBLIC_SERVER_URL}/f/${frame.FILE_ID}`}]:[]
            });
          });
          layout_docs.push(ite);
        }
       
        setDocs(layout_docs)
  }, [props.attached_files]);


  useEffect(() => {
    if(isShowPop){
      document.body.style.overflow = 'hidden';
      setDefaultActive("t03")
    }else{
      document.body.style.overflow = 'unset';
       setDefaultActive("t01")
    }
  }, [isShowPop]);


  const getBtnStatus = (STATUS)=>{
     switch(STATUS){
        case "YCBS":
          return  language.key_YCBS
        case "DYKH":
          return  language.key_DYKH
        case "TCKH":
          return  language.key_TCKH
        default:
          return null
      }
  }

  const onClickBtn = ()=>{
    props.onClickBtnConfirm(props.info.STATUS)
    setLoading(true)
    setTimeout(
      () => {
        setLoading(false)
        
      }, 
      600
    );
  }

  // const handleScroll = (e) => {
  //   const bottom = e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight;
  //   if(e.target.scrollTop == 0){
  //     if(showArrow===true){
  //       console.log("nhay vao 1")
  //       setShowArrow(false)
  //     }
  //   }else if(bottom){
  //     if(showArrow===false){
  //       console.log("nhay vao 2")
  //       setShowArrow(true)
  //     }
      
  //   }
  // }
  const formatCurrency = (value) => {
    return currencyFormatter.format(value, {code: l.g("bhsk.currency"),precision: 0,format: "%v %s",symbol: l.g("bhsk.currency")});
   };

  const onClickScroll = ()=>{
    scroll_ref.scrollTo({ behavior: "smooth" });
  }



  const Tab1 = ({})=>{
    return (<div className="bb-82-89i2">
      <div className="row">
        <div className="col-md-4">
          <div className="ad-092-x12">
            <div className="lx12">{language.product_name}:</div>
            <div className="lx13">{props.info.PRODUCT_NAME}</div>
          </div>
          <div className="ad-092-x12">
            <div className="lx12">{language.request_name}:</div>
            <div className="lx13">{props.info.REQUIRE_NAME}</div>
          </div>
          <div className="ad-092-x12">
            <div className="lx12">{language.status}:</div>
            <div className="lx13">{props.info.STATUS_NAME}</div>
          </div>
        </div>        
        <div className="col-md-4">
          <div className="ad-092-x12">
            <div className="lx12">{language.issuer}:</div>
            <div className="lx13">{props.info.NAME}</div>
          </div>
          <div className="ad-092-x12">
            <div className="lx12">{language.request_date}:</div>
            <div className="lx13">{props.info.CLAIM_DATE}</div>
          </div>
        </div>        
        <div className="col-md-4">
          
          {getBtnStatus(props.info.STATUS)&&<button disabled={loading} className="confirm-bt" onClick={()=>onClickBtn()}>

            <div className="loading-title-btn">
            {loading&&<div class="spinner-box">
              <div class="pulse-container">  
                <div class="pulse-bubble pulse-bubble-1"></div>
                <div class="pulse-bubble pulse-bubble-2"></div>
                <div class="pulse-bubble pulse-bubble-3"></div>
              </div>
            </div>}
            <span className={loading&&"txt-lb"}>{getBtnStatus(props.info.STATUS)}</span>
            </div>
            </button>}
        </div>        
      </div>

    </div>)
  }
  const Tab2 = ({})=>{
    return (<div className="bb-82-89i2">
      <div className="row">
        <div className="col-md-6">
          <div className="ad-092-x12">
            <div className="lx12">{language.gnc_no}:</div>
            <div className="lx13">{props.info.CERTIFICATE_NO}</div>
          </div>
          <div className="ad-092-x12">
            <div className="lx12">{language.amount_claim}:</div>
            <div className="lx13">{formatCurrency(props.info.REQUIRED_AMOUNT)}</div>
          </div>
          
        </div>        
        <div className="col-md-6">
          <div className="ad-092-x12">
            <div className="lx12">{language.time_iss}:</div>
            <div className="lx13">{props.info.EFFECTIVE_DATE} - {props.info.EXPIRATION_DATE}</div>
          </div>
          <div className="ad-092-x12">
            <div className="lx12">{language.pay_method}:</div>
            <div className="lx13">{props.info.PAYMENT_NAME}</div>
          </div>
        </div>        
      </div>

    </div>)
  }
  const Tab3 = ({})=>{
    const lang_up_file = {
      "select_file": language.select_file,
      "no_file": language.no_file,
      "delete": language.delete
    }
    return (<div>
      <UploadFrame viewOnly={true} data={docs} col_layout={2} language={lang_up_file}/>
    </div>)
  }
  const Tab4 = ({})=>{
    return (<div className="progress-contn">
      
     <div>
      <ScrollContainer className="wrapper-progress" horizontal={false} draggingClassName={"dragging-doggy"}>
      <div ref={(el) => { scroll_ref = el; }} >
      {props.progress.map((process, index)=>{
          if(index == 0){
            return (<div className="step-item-l active">
                    <div className="title-progress">
                        <div className="number">{props.progress.length}</div>
                        <div className="lb-k">{process.WORK_NAME}</div>
                    </div>
                    <div className="content-progress">
                      {process.CONTENT}
                    </div>
                </div>)
          }else{
            return ( <div className="step-item-l effect-shine">
            <div className="title-progress">
                <div className="number"><i class="fas fa-check"></i></div>
                <div className="lb-k">{process.WORK_NAME}</div>
            </div>
            <div className="content-progress">
             
            </div>
        </div>)
          }
          
      })}
      </div>
       
      <div className="fade-top-prg"></div>
      <div className="fade-bottom-prg">
        {props.progress.length>4&&<div class="center-con">
            <div class="round">
                <div id="ctaxxxx">
                <div class="arrow fas fa-angle-down primera next"></div>
                <div class="arrow fas fa-angle-down segunda next"></div>
                   
                </div>
            </div>
        </div>}



      </div>
      </ScrollContainer>
     </div>


    </div>)
  }


  const closeMobilePopup = ()=>{
    
    setExpand(false)
    setTimeout(
    () =>setShowPop(false), 
    300
  );
  }
  if(props.show){

  }
  return (
    props.show?
    <div className="claim-result-box" ref={ref}>
        <div className="_claim_header_">
          <div className="j392">
            <img src="/img/eclaim/insureer.png" />
            <div className="hsj123k">
              <div className="k1">{language.cer_no}:</div>
              <div className="k2">{props.info.CLAIM_NO}</div>
            </div>
          </div>



        </div>

        <div className={`h2092-body mobile-${isMobile}`}>
          <Tab1/>

        <div className="result-pop-container">
            {(isMobile && isShowPop)&&<div className="overlay"/>}
            <div className="ab-2332-3kl">


            {(isMobile && isShowPop)&&<div className="top-k21lo">
              <div className="j392">
                <img src="/img/eclaim/insureer.png" />
                <div className="hsj123k">
                  <p className="k1">{language.cer_no}:</p>
                  <p className="k2">{props.info.CONTRACT_NO}</p>
                </div>

                <div className="btn-close-x" onClick={()=>{closeMobilePopup()}}><i class="fas fa-times"></i> Đóng</div>
              </div>


            </div>}


            <div className="abbnn212">
                <Accordion activeKey={expand?"0":"1"}>
                  <Accordion.Collapse eventKey="0">
                    <div className="jwt-202">
                      <div className="par2-io02">

                      <div className="row">

                        <div className="col-md-8">
                          <Tab.Container id="left-tabs-example" defaultActiveKey={defaultActive} className="rodw-0283">
                           
                                <Nav variant="pills" className="row custom-tabs">

                                  {isMobile&&<Nav.Item>
                                    <Nav.Link eventKey="t03">{language.request}</Nav.Link>
                                  </Nav.Item>}

                                  <Nav.Item>
                                    <Nav.Link eventKey="t01">{language.info}</Nav.Link>
                                  </Nav.Item>

                                  <Nav.Item>
                                    <Nav.Link eventKey="t02">{language.document}</Nav.Link>
                                  </Nav.Item>


                                  {isMobile&&<Nav.Item>
                                    <Nav.Link eventKey="t04">{language.progress}</Nav.Link>
                                  </Nav.Item>}

                                </Nav>
                              
                                <Tab.Content>
                                  {isMobile&&<Tab.Pane eventKey="t03">
                                     <Tab1/>
                                  </Tab.Pane>}

                                  <Tab.Pane eventKey="t01">
                                     <Tab2/>
                                  </Tab.Pane>
                                  <Tab.Pane eventKey="t02">
                                     <Tab3/>
                                  </Tab.Pane>
                                  {isMobile&&<Tab.Pane eventKey="t04">
                                     <Tab4/>
                                  </Tab.Pane>}
                                </Tab.Content>
                             
                          </Tab.Container>
                       </div>


                       {!isMobile&&<div className="col-md-4">
                          <div className="ajs823-0293">
                            <span>{language.progress}</span>
                          </div>
                          <Tab4/>
                       </div>}

                      </div>



                       
                       
                       
                       
                      </div>
                    </div>
                   
                  </Accordion.Collapse>
                </Accordion>
            </div>


        </div>
      </div>



        <div className="collapsed-ed">
            <div className="js82-021" onClick={()=>{
              if(isMobile){
                  setShowPop(true)
                  setExpand(true)
              }else{
                setExpand(!expand)
              }
              
            }}>{expand?language.cls:language.claim_detail} <i class={expand?"fas fa-chevron-up":"fas fa-chevron-down"}></i></div>
        </div>

          



        </div>
    </div>:<div>
      

      <div className="search-not-found">
        <div className="conta-not-found">
            <div className="iconx-found">
              <i class="far fa-frown"></i>
            </div>
            <div className="cota-content">
              <div className="text-notejs">{language.no_result}</div>
              <div className="note-text">{language.noresult_note}</div>
            </div>

        </div>  

      </div>


    </div>
  );
})

// Hook
function useWindowSize() {
  // Initialize state with undefined width/height so server and client renders match
  // Learn more here: https://joshwcomeau.com/react/the-perils-of-rehydration/
  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined,
  });
  useEffect(() => {
    // Handler to call on window resize
    function handleResize() {
      // Set window width/height to state
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    }
    // Add event listener
    window.addEventListener("resize", handleResize);
    // Call handler right away so state gets updated with initial window size
    handleResize();
    // Remove event listener on cleanup
    return () => window.removeEventListener("resize", handleResize);
  }, []); // Empty array ensures that effect is only run on mount
  return windowSize;
}

export default ClaimResult;
