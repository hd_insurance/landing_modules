///Modal xac nhan tu choi boi thuong

import React, { useEffect, useState, createRef, useRef, forwardRef } from "react";
import { animateScroll } from "react-scroll";
import DynamicRender from "../../../components/render";
import { confirmAlert } from "react-confirm-alert";
import api from "../../../services/Network.js";
import moment, { now } from "moment";
import cogoToast from "cogo-toast";
import currencyFormatter from "currency-formatter";
import util from "../../../components/render/util";
import {LoadingComponent} from "hdi-uikit";


const Modal1 = forwardRef((props, ref) => {
  const [internal_lang, setInternalLang] = useState([]);
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState(props.data);
  const [define, setDefine] = useState({});
  const [listmaped, setMaped] = useState({});
  const [show, setShow] = useState(false);
  const [statusState, setStatusState] = useState(null);
  const [refresh, setRefresh] = useState("0");
  const [loadingSubmit, setLoadinSubmit] = useState(false);
  const [otpConfig, setOTPConfig] = useState({
    claimCode: props.claimCode,
    showReason: false,
    contact: props.data.email
  });
  const [state, setState] = useState({
    view_singleupload: {hide: true},
    view_btn_confirm: {hide: false},
    view_div_success: {hide: true},
    view_btn_close: {hide: true},
    view_prev_button: {hide: true}
  })
  const [status, setStatus] = useState("-1");

  //modal
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
      const { obj_config, map } = util.rootObjectComponent(props.layout, state);
      setMaped(map);
      setDefine(obj_config);
      if(props.layout.internal_language){
        setInternalLang(props.layout.internal_language)
      }
      setTimeout(() => {

        setLoading(false);
      }, 200);

  }, []);

  useEffect(() => {
    if (!util.isEmptyObj(listmaped)) { 
     listmaped["btn_verify"].setHidden(true)
     listmaped["btn_reject"].setHidden(false)
     listmaped["btn_confirm"].setHidden(false)

      listmaped["otp_confirm"].setHidden(true)
      listmaped["div_info"].setHidden(false)


    var ttam = 0
     data.amount_indemnify.forEach((ite, id)=>{
        ttam+=ite.AMOUNT
     })
    ttam = amountFormat(ttam) //txt_accept_amount
    listmaped["require_mount"].define.value = data.require_amount
    listmaped["txt_accept_amount"].define.value = ttam
    setRefresh(new Date().getTime())

    }
  },[listmaped]);


  useEffect(() => {
      setData(props.data)
  }, [props.data]);

  const onConfirmClick = ()=>{
    listmaped["otp_confirm"].setHidden(false)
    listmaped["btn_verify"].setHidden(false)
    listmaped["btn_reject"].setHidden(true)
    listmaped["btn_confirm"].setHidden(true)
    listmaped["div_info"].setHidden(true)
    listmaped["otp_confirm"].define.showReason = false
    otpConfig.showReason = false
    setOTPConfig({...otpConfig})
    setStatus("1")
    setStatusState(1)
    
  }


  const showSuccesUpdate = ()=>{
    listmaped["otp_confirm"].setHidden(true)
    listmaped["div_success"].setHidden(false)
    listmaped["btn_close"].setHidden(false)
    setState((prevState) => {
      prevState.view_btn_close = {hide: false}
      prevState.view_div_success = {hide: false}
      prevState.view_prev_button = {hide: true}
      prevState.view_btn_verify = {hide: true}
      prevState.view_btn_verify = {hide: true}
      return prevState
    })

    if(statusState==1){
      listmaped["txt_mess_md"].define.value = internal_lang.ms_accept_1
    }else{
      listmaped["txt_mess_md"].define.value = internal_lang.ms_reject_1
    }
    setRefresh(new Date().getTime())
  }



  const onVerifyClick = ()=>{
    updateStatusClaim(statusState==1?true:false)
  }
  const onRejectClick = ()=>{
    
    listmaped["btn_verify"].setHidden(false)
    listmaped["btn_reject"].setHidden(true)
    listmaped["btn_confirm"].setHidden(true)
    listmaped["otp_confirm"].setHidden(false)
    listmaped["div_info"].setHidden(true)
    otpConfig.showReason = true
    setOTPConfig({...otpConfig})
    setStatus("0")
    setStatusState(0)
   
  }
  const onTabSelect = (name, value)=>{
    setStep(value)
  }


   const updateStatusClaim = async (isAccept)=>{
    if(loadingSubmit){
      return;
    }
     setLoadinSubmit(true)
     try{
        const otp_data = listmaped["otp_confirm"].ref.current.getOtpData()
        if(otp_data){
          // showSuccesUpdate()
          const reqdata = {
            content: otp_data.reason,
            otp: otp_data.otp,
            accept: isAccept
          }
          const result = await api.post(`/api/eclaim/status/${props.data.current_product}/${props.claimCode}`, reqdata)
          if(result.success){
            if(result.otp){
              if(result.submit){
                showSuccesUpdate()
              }else{
                 cogoToast.error(result.message)
              }
            }else{
               cogoToast.error(internal_lang.ms_otp_incorrect)
            }
          }else{
            cogoToast.error("Lỗi gửi yêu cầu")
          }
          
        }
        setLoadinSubmit(false)
      }catch(e){
        console.log(e)
        setLoadinSubmit(false)
        cogoToast.error("Request is error")
      }
  }


  const amountFormat = (am)=>{
    return currencyFormatter.format(am, {
            code: l.g("bhsk.currency"),
            precision: 0,
            format: "%v %s",
            symbol: l.g("bhsk.currency"),
          })
  }
  return (
    <div>
       {loading ? (
        <div >
          <div>
            Please wait..
          </div>
        </div>
      ) : (<div>
            <LoadingComponent loading={loadingSubmit}>
              <DynamicRender
                layout={define}
                status={status}
                refresh={refresh}
                otpConfig={otpConfig}
                onTabSelect={onTabSelect}
                onConfirmClick={onConfirmClick}
                onVerifyClick={onVerifyClick}
                onRejectClick={onRejectClick}
                onCloseClick={props.onCloseClick}
              />
            </LoadingComponent>


            </div>)}
    </div>
  );
})


export default Modal1;
