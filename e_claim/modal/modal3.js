///Modal bo sung giấy tờ

import React, { useEffect, useState, createRef, useRef, forwardRef } from "react";
import { animateScroll } from "react-scroll";
import DynamicRender from "../../../components/render";
import { confirmAlert } from "react-confirm-alert";
import api from "../../../services/Network.js";
import moment, { now } from "moment";
import cogoToast from "cogo-toast";
import currencyFormatter from "currency-formatter";
import util from "../../../components/render/util";
import {LoadingComponent} from "hdi-uikit";

var _ = require("lodash");

const Modal1 = forwardRef((props, ref) => {
  const [bene_type, setBeneType] = useState(null);
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState(props.data);
  const [define, setDefine] = useState({});
  const [listmaped, setMaped] = useState({});
  const [show, setShow] = useState(false);
  const [refresh, setRefresh] = useState("0");
  const [layoutUpload, setLayoutUpload] = useState([]);
  const [file_attach, setFileAttach] = useState([]);
  const [loadingSubmit, setLoadinSubmit] = useState(false);
  const [state, setState] = useState({
    view_div_success: {hide: true},
    view_btn_close: {hide: true},
    view_div_upload: {hide: false},
    view_btn_close: {hide: true},
    view_btn_confirm: {hide: false}

  })
  const [internal_lang, setInternalLang] = useState([]);
  //modal
  const [showModal, setShowModal] = useState(false);

   useEffect(() => {
      setData(props.data)

        const attached_files_data = props.data.attached_files
        var grouped = _.mapValues(_.groupBy(attached_files_data, "GROUP_ID"), (clist) =>
          clist.map((car) => _.omit(car, "GROUP_ID"))
        );

       
        var layout_docs = []
        for (const [key, value] of Object.entries(grouped)) {
          let ite = {
            "group_name": value[0].GROUP_NAME,
            "GROUP_ID": key,
            "files":[]
          }
          value.forEach((frame, index) => {
            ite.files.push({
              "id": index+"-"+key.toString(),
              "label": frame.FILE_NAME,
              "key": frame.FILE_KEY,
              "description": frame.GROUP_DESC,
              "required": true,
              "max": 10,
              "list":[]
            });
          });
          layout_docs.push(ite);
        }
      
      setLayoutUpload(layout_docs);

  }, [props.data]);

  useEffect(() => {
      const { obj_config, map } = util.rootObjectComponent(props.layout, state);
      if(props.layout.internal_language){
        setInternalLang(props.layout.internal_language)
      }
      setMaped(map);
      setDefine(obj_config);
      setTimeout(() => {
        setLoading(false);
      }, 200);

  }, []);

  useEffect(() => {
    if (!util.isEmptyObj(listmaped)) { 
        
        setValueScreen1()
        setValueScreen2()
        setValueScreen3()
        setRefresh(new Date().getTime())
    }
  },[listmaped]);


  const setValueScreen1 = ()=>{
    
  }
  const setValueScreen2 = ()=>{
  }
  const setValueScreen3 = ()=>{
  }

 

  const showSuccesUpdate = ()=>{
    listmaped["div_success"].setHidden(false)
    listmaped["btn_close"].setHidden(false)
    setState((prevState) => {
        prevState.view_btn_close = {hide: false}
        prevState.view_btn_confirm = {hide: true}
        prevState.view_div_success = {hide: false}
        prevState.view_div_upload = {hide: true}
      return prevState
    })
    listmaped["txt_mess_md"].define.value = internal_lang.ms_success_1
    setRefresh(new Date().getTime())
  }

 
  const onConfirmClick = async ()=>{
    if(!loadingSubmit){
       setLoadinSubmit(true)
       try{
         const result = await api.post(`/api/eclaim/document/${props.data.current_product}/${props.claimCode}`, file_attach)
         if(result.success){
          showSuccesUpdate()
         }
         setLoadinSubmit(false)
        }catch(e){
          setLoadinSubmit(false)
          cogoToast.error("Request is error")
        }
    }
  }

  const onFileUpdate = (file_attachment)=>{
    if(file_attachment){
      setFileAttach(file_attachment)
    }else{
      cogoToast.error("File upload is error!")
    }
  }




  return (
    <div>
       {loading ? (
        <div >
          <div>
            Please wait..
          </div>
        </div>
      ) : (<LoadingComponent loading={loadingSubmit}>
            <DynamicRender
              refresh={refresh}
              layout={define}
              upload_layout={layoutUpload}
              onConfirmClick={onConfirmClick}
              onFileUpdate={onFileUpdate}
              onCloseClick={props.onCloseClick}
            />
            </LoadingComponent>)}
    </div>
  );
})


export default Modal1;
