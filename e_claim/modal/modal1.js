///Modal xac nhan phuong an boi thuong

import React, { useEffect, useState, createRef, useRef, forwardRef } from "react";
import { animateScroll } from "react-scroll";
import DynamicRender from "../../../components/render";
import { confirmAlert } from "react-confirm-alert";
import api from "../../../services/Network.js";
import moment, { now } from "moment";
import cogoToast from "cogo-toast";
import currencyFormatter from "currency-formatter";
import util from "../../../components/render/util";
import {LoadingComponent} from "hdi-uikit";



const Modal1 = forwardRef((props, ref) => {
  const [bene_type, setBeneType] = useState(null);
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState(props.data);
  const [define, setDefine] = useState({});
  const [listmaped, setMaped] = useState({});
  const [show, setShow] = useState(false);
  const [refresh, setRefresh] = useState("0");
  const [step, setStep] = useState("0");
  const [issuser_beneficiary, setIssuerBene] = useState({});
  const [issuser_bank, setIssuerBank] = useState({});
  const [statusState, setStatusState] = useState(null);
  const [beneFile, setBeneFile] = useState([]);
  const [loadingSubmit, setLoadinSubmit] = useState(false);
  const [otpConfig, setOTPConfig] = useState({
    claimCode: props.claimCode,
    showReason: false,
    contact: props.data.email
  });
  const [state, setState] = useState({
    view_singleupload: {hide: true},
    view_bene_sumary_edit: {hide: true},
    view_payment_sumary_edit: {hide: true},
    view_payment_sumary: {hide: false},
    view_div_success: {hide: true},
    view_btn_close: {hide: true},
    view_prev_button: {hide: true}
  })
  const [internal_lang, setInternalLang] = useState([]);
  //modal
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
      setData(props.data)
  }, [props.data]);

  useEffect(() => {
      const { obj_config, map } = util.rootObjectComponent(props.layout, state);
      if(props.layout.internal_language){
        setInternalLang(props.layout.internal_language)
      }
      setMaped(map);
      setDefine(obj_config);
      setTimeout(() => {
        setLoading(false);
      }, 200);

  }, []);

  useEffect(() => {
    if (!util.isEmptyObj(listmaped)) { 
        listmaped["otp_confirm"].setHidden(true)
        listmaped["bst_tabs"].setHidden(false)
        listmaped["btn_next"].setHidden(false)
        listmaped["btn_reject"].setHidden(false)
        listmaped["btn_confirm"].setHidden(true)
        listmaped["btn_next"].define.value = "Tiếp tục"
        setValueScreen1()
        setValueScreen2()
        setValueScreen3()
        setRefresh(new Date().getTime())
    }
  },[listmaped]);


  const setValueScreen1 = ()=>{
    var ttam = 0
     data.amount_indemnify.forEach((ite, id)=>{
        ttam+=ite.AMOUNT
     })
    ttam = amountFormat(ttam) //txt_accept_amount
    listmaped["require_mount"].define.value = data.require_amount
    listmaped["txt_accept_amount"].define.value = ttam

  }
  const setValueScreen2 = ()=>{
    listmaped["txt_issuer_name"].define.value = data.beneciary_info.NAME
    listmaped["txt_bene_type"].define.value = data.beneciary_info.BENEFICIARY_TYPE_NAME
    if(data.beneciary_info.BENEFICIARY_TYPE != "INSURER"){
        setState((prevState) => {
          prevState.view_div_bene_phone = {hide: false}
          prevState.view_div_bene_passport = {hide: false}
          prevState.view_div_bene_name = {hide: false}
          
          prevState.view_txt_bene_name = {value: data.beneciary_info.BENEFICIARY_NAME}
          prevState.view_txt_bene_phone = {value: data.beneciary_info.BENEFICIARY_PHONE}
          prevState.view_txt_bene_passport = {value: data.beneciary_info.BENEFICIARY_IDCARD}
          return prevState
        })
    }else{
       setState((prevState) => {
          prevState.view_div_bene_phone = {hide: true}
          prevState.view_div_bene_passport = {hide: true}
          prevState.view_div_bene_name = {hide: true}
          return prevState
        })
    }

  }
  const setValueScreen3 = ()=>{
     setState((prevState) => {
      prevState.view_txt_paymethod = {value: data.bank_info.PAYMENT_NAME}
      prevState.view_txt_bank_name = {value: data.bank_info.ACCOUNT_BANK}
      prevState.view_txt_bank_account = {value: data.bank_info.ACCOUNT_NAME}
      prevState.view_txt_bank_number = {value: data.bank_info.ACCOUNT_NO}
      if(data.bank_info.SWIFT_CODE){
        prevState.view_div_bank_swift = {hide: false}
        prevState.view_txt_bankswift = {value: data.bank_info.SWIFT_CODE}
        prevState.view_div_bene_address = {hide: false}
        prevState.view_txt_bene_address = {value: data.bank_info.BENEFICIARY_ADDRESS}


        if(data.bank_info.INTER_SWIFT_CODE && data.bank_info.INTERMEDIATE_BANK){
          prevState.view_div_midle_bank_name = {hide: false}
          prevState.view_txt_mid_bankname = {value: data.bank_info.INTERMEDIATE_BANK}
          prevState.view_div_midle_bank_swift = {hide: false}
          prevState.view_txt_mid_bankswift = {value: data.bank_info.INTER_SWIFT_CODE}
        }else{
          prevState.view_div_midle_bank_name = {hide: true}
          prevState.view_div_midle_bank_swift = {hide: true}
        }
      }else{
        prevState.view_div_midle_bank_name = {hide: true}
        prevState.view_div_bank_swift = {hide: true}
        prevState.view_div_bene_address = {hide: true}
        prevState.view_div_midle_bank_swift = {hide: true}
      }
      return prevState
    })
  }

  const onBeneChange = (vl)=>{
    
    if(vl == "BENEFICIARY_POINT" || vl == "HEIR"){
      setState((prevState) => {
          prevState.view_singleupload = {hide: false}
          return prevState
      })
    }else{
      setState((prevState) => {
          prevState.view_singleupload = {hide: true}
          return prevState
      })
    }
    setBeneType(vl)
  }

  const onNextClick = ()=>{

    if(step==2){
      listmaped["btn_next"].setHidden(true)
      listmaped["btn_reject"].setHidden(true)
      listmaped["btn_confirm"].setHidden(false)
      listmaped["otp_confirm"].setHidden(false)
      listmaped["bst_tabs"].setHidden(true)
      listmaped["otp_confirm"].define.showReason = false
      otpConfig.showReason = false
      otpConfig.contact = props.data.EMAIL
      setOTPConfig({...otpConfig})
      setState((prevState) => {
        prevState.view_prev_button = {hide: true}
        return prevState
      })
      setRefresh(new Date().getTime())
      setStatusState(1)
    }
    if(step == 0){
      setValueScreen2()
    }
    if(step==1){
      listmaped["btn_next"].define.value = "Hoàn thành"
    }
    if((step*1) < 2){
     setState((prevState) => {
        prevState.view_prev_button = {hide: false}
        return prevState
    })
     setStep(((step*1) + 1).toString())
    }


  }
  const onPreviousClick = ()=>{
    listmaped["btn_next"].define.value = "Tiếp tục"
    if((step*1)==1){
      setState((prevState) => {
        prevState.view_prev_button = {hide: true}
        return prevState
      })
    }
    if((step*1)>0){
      setStep(((step*1) - 1).toString())
    }
  }
  const onCancelClick1 = ()=>{
    listmaped["btn_next"].define.disabled = false
    listmaped["btn_reject"].define.disabled = false
     setState((prevState) => {
        prevState.view_bene_sumary = {hide: false}
        prevState.view_bene_sumary_edit = {hide: true}
        prevState.view_singleupload = {hide: true}
        return prevState
    })
    setRefresh(new Date().getTime())

  }
  const onCancelClick2 = ()=>{
    listmaped["btn_next"].define.disabled = false 
    listmaped["btn_reject"].define.disabled = false
    setState((prevState) => {
        prevState.view_payment_sumary_edit = {hide: true}
        prevState.view_payment_sumary = {hide: false}
        return prevState
    })
    setRefresh(new Date().getTime())

  }
  const onTabSelect = (name, value)=>{
    if(value==2){
      listmaped["btn_next"].define.value = "Hoàn thành"
    }else{
      listmaped["btn_next"].define.value = "Tiếp tục"
    }
    if(value==0){
      setState((prevState) => {
        prevState.view_prev_button = {hide: true}
        return prevState
      })
    }else{
      setState((prevState) => {
        prevState.view_prev_button = {hide: false}
        return prevState
      })
    }
    
    setStep(value)
  }

  const onEditBeneClick = ()=>{
    listmaped["btn_next"].define.disabled = true
    listmaped["btn_reject"].define.disabled = true
    setState((prevState) => {
        prevState.view_bene_sumary = {hide: true}
        prevState.view_bene_sumary_edit = {hide: false}
        return prevState
    })
    setRefresh(new Date().getTime())
  }
  const onEditPaymentClick = ()=>{
    listmaped["btn_next"].define.disabled = true
    listmaped["btn_reject"].define.disabled = true
    setState((prevState) => {
        prevState.view_bene_sumary = {hide: false}
        prevState.view_bene_sumary_edit = {hide: true}
        prevState.view_payment_sumary_edit = {hide: false}
        prevState.view_payment_sumary = {hide: true}
        return prevState
    })
    setRefresh(new Date().getTime())
  }

  const updateBene = (user_beneficiary)=>{
    if(loadingSubmit){
      return;
    }
    setLoadinSubmit(true)
    return new Promise(async (resolve, rejected)=>{
      try{
        const data_bene = {
            "channel": "WEB",
            "claim_code": props.claimCode,
            "type": "BAN_THAN",
            "claim": {
              "beneficiary": {            
                "beneficiary_type": user_beneficiary.bene_type,
                "beneficiary_gender": user_beneficiary.bene_type!="INSURER"?user_beneficiary.gender:"",
                "beneficiary_name": user_beneficiary.bene_type!="INSURER"?user_beneficiary.name.toUpperCase():"",
                "beneficiary_phone": user_beneficiary.bene_type!="INSURER"?user_beneficiary.phone:"",
                "beneficiary_idcard": user_beneficiary.bene_type!="INSURER"?user_beneficiary.passport:"",
                "beneficiary_address": "Địa chỉ thụ hưởng"
              },
              "file_attach": user_beneficiary.bene_type!="INSURER"?beneFile:[]
            }
          }

          const update_result = await api.post(`/api/eclaim/update/${props.data.current_product}/${props.claimCode}`, data_bene)
          cogoToast.success(internal_lang.ms_update_success)
          setLoadinSubmit(false)
          return resolve(true)
      }catch(e){
        console.log(e)
        setLoadinSubmit(false)
        cogoToast.error(internal_lang.ms_error_request)
        return resolve(false)
      }
    })
  }

  const updatePayment = (bank_beneficiary)=>{
    if(loadingSubmit){
      return;
    }
    setLoadinSubmit(true)
    return new Promise(async (resolve, rejected)=>{
      try{
        const data_bene = {
            "channel": "WEB",
            "claim_code": props.claimCode,
            "type": "BAN_THAN",
            "claim": {
              "payment": {            
                "payment_type": bank_beneficiary.transfer_type=="STK"?"CK":"",
                "account_no": bank_beneficiary.bank_number,
                "account_name": bank_beneficiary.bank_account,
                "account_bank": bank_beneficiary.bank_name,
                "branch_bank": bank_beneficiary.bank_type=="ND"?bank_beneficiary.bank_branch:"",
                "swift_code":  bank_beneficiary.bank_type=="QT"?bank_beneficiary.bank_swiffcode:"",
                "intermediate_bank": bank_beneficiary.isMiddle?bank_beneficiary.middle_bank_name:"",
                "inter_swift_code": bank_beneficiary.isMiddle?bank_beneficiary.middle_swiff:"",
                "bank_address": bank_beneficiary.bank_type=="QT"?bank_beneficiary.bank_address:"",
                "beneficiary_address": bank_beneficiary.bank_type=="QT"?bank_beneficiary.account_address:"",
              }
            }
          }

          const update_result = await api.post(`/api/eclaim/update/${props.data.current_product}/${props.claimCode}`, data_bene)
          cogoToast.success(internal_lang.ms_update_success)
          setLoadinSubmit(false)
          return resolve(true)
      }catch(e){
        setLoadinSubmit(false)
        cogoToast.error(internal_lang.ms_error_request)
        return resolve(false)
      }
    })
  }

  const onSaveClick1 = async ()=>{
  
    if(listmaped["bene_sumary_edit"].ref.current.handleSubmit()){
        const user_beneficiary = listmaped["user_beneficiary"].ref.current.getData();
        await updateBene(user_beneficiary)
        setIssuerBene(user_beneficiary)

        

        if(user_beneficiary.bene_type != "INSURER"){
          setState((prevState) => {
              prevState.view_div_bene_phone = {hide: false}
              prevState.view_div_bene_passport = {hide: false}
              prevState.view_div_bene_name = {hide: false}

              prevState.view_txt_bene_type = {value: user_beneficiary.bene_label}
              prevState.view_txt_bene_name = {value: user_beneficiary.name.toUpperCase()}
              prevState.view_txt_bene_phone = {value: user_beneficiary.phone}
              prevState.view_txt_bene_passport = {value: user_beneficiary.passport.toUpperCase()}
              return prevState
          })





        }else{
          setState((prevState) => {
              prevState.view_txt_bene_type = {value: user_beneficiary.bene_label}
              prevState.view_div_bene_phone = {hide: true}
              prevState.view_div_bene_passport = {hide: true}
              prevState.view_div_bene_name = {hide: true}
              return prevState
          })
        }

      onCancelClick1()
    }

  }

  const onSaveClick2 = async ()=>{
    if(listmaped["payment_sumary_edit"].ref.current.handleSubmit()){
      const bank_beneficiary = listmaped["bank_beneficiary"].ref.current.getData();
      setIssuerBank(bank_beneficiary)
      await updatePayment(bank_beneficiary)
      setState((prevState) => {
          prevState.view_txt_paymethod = {value: bank_beneficiary.pay_method_label}
          prevState.view_txt_bank_type = {value: bank_beneficiary.bank_type_label}
          prevState.view_txt_bank_name = {value: bank_beneficiary.bank_name}
          prevState.view_txt_bank_account = {value: bank_beneficiary.bank_account.toUpperCase()}
          prevState.view_txt_bank_number = {value: bank_beneficiary.bank_number}
          if(bank_beneficiary.bank_type == "QT"){
            prevState.view_div_bank_swift = {hide: false}
            prevState.view_txt_bankswift = {value: bank_beneficiary.bank_swiffcode}
            prevState.view_div_bene_address = {hide: false}
            prevState.view_txt_bene_address = {value: bank_beneficiary.account_address}


            if(bank_beneficiary.isMiddle){
              prevState.view_div_midle_bank_name = {hide: false}
              prevState.view_txt_mid_bankname = {value: bank_beneficiary.middle_bank_name}
              prevState.view_div_midle_bank_swift = {hide: false}
              prevState.view_txt_mid_bankswift = {value: bank_beneficiary.middle_swiff}
            }else{
              prevState.view_div_midle_bank_name = {hide: true}
              prevState.view_div_midle_bank_swift = {hide: true}
            }
          }else{
            prevState.view_div_midle_bank_name = {hide: true}
            prevState.view_div_bank_swift = {hide: true}
            prevState.view_div_bene_address = {hide: true}
            prevState.view_div_midle_bank_swift = {hide: true}
          }


          return prevState
      })

      onCancelClick2()
    }
  }

  const onRejectClick = ()=>{
    listmaped["otp_confirm"].setHidden(false)
    listmaped["bst_tabs"].setHidden(true)
    listmaped["btn_next"].setHidden(true)
    listmaped["btn_reject"].setHidden(true)
    listmaped["btn_confirm"].setHidden(false)
    listmaped["otp_confirm"].define.showReason = false
    otpConfig.showReason = true
    otpConfig.contact = props.data.EMAIL
    setState((prevState) => {
        prevState.view_prev_button = {hide: true}
        return prevState
    })

    setOTPConfig({...otpConfig})
    setStatusState(0)
  }

  const showSuccesUpdate = ()=>{
    listmaped["otp_confirm"].setHidden(true)
    listmaped["div_success"].setHidden(false)
    listmaped["btn_close"].setHidden(false)
    listmaped["btn_confirm"].setHidden(true)
    setState((prevState) => {
      prevState.view_btn_close = {hide: false}
      prevState.view_div_success = {hide: false}
      prevState.view_prev_button = {hide: true}
      return prevState
    })

    if(statusState==1){
      listmaped["txt_mess_md"].define.value = internal_lang.ms_accept_1
    }else{
      listmaped["txt_mess_md"].define.value = internal_lang.ms_reject_1
    }
    setRefresh(new Date().getTime())
  }

  const updateStatusClaim = async (isAccept)=>{
    if(loadingSubmit){
      return;
    }
    setLoadinSubmit(true)
     try{
        const otp_data = listmaped["otp_confirm"].ref.current.getOtpData()
        if(otp_data){
          const reqdata = {
            content: otp_data.reason,
            otp: otp_data.otp,
            accept: isAccept
          }
          const result = await api.post(`/api/eclaim/status/${props.data.current_product}/${props.claimCode}`, reqdata)
          if(result.success){
            if(result.otp){
              if(result.submit){
                showSuccesUpdate()
              }else{
                 cogoToast.error(result.message)
              }
            }else{
               cogoToast.error(internal_lang.ms_otp_incorrect)
            }
          }else{
            cogoToast.error("Lỗi gửi yêu cầu")
          }
          
        }
        setLoadinSubmit(false)
      }catch(e){
        setLoadinSubmit(false)
        console.log(e)
        cogoToast.error("Request is error")
      }
  }
  const onConfirmClick = ()=>{
     updateStatusClaim(statusState==1?true:false)
  }



  const amountComponent = (props)=>{
    return <div>
     { props.amount_data.map((ite, id)=>{
        return(<div class="row mt-15">
          <div class="col-md-6 ite-left ">{ite.BEN_NAME}</div>
          <div class="col-md-6 text-red bold ">{amountFormat(ite.AMOUNT)}</div>
          </div>)
      })}

    </div>
  }
  const amountFormat = (am)=>{
    return currencyFormatter.format(am, {
            code: l.g("bhsk.currency"),
            precision: 0,
            format: "%v %s",
            symbol: l.g("bhsk.currency"),
          })
  }
  const onFileUpdate = (file_atm)=>{
    if(file_atm){
      const newf = file_atm.map((item, index)=>{
        item.TYPE_OF_PAPER = "0"
        return item
      })
      setBeneFile(newf)
    }
    
  }

  return (
    <div>
       {loading ? (
        <div >
          <div>
            Please wait..
          </div>
        </div>
      ) : (<div>
            <LoadingComponent loading={loadingSubmit}>
            <DynamicRender
              refresh={refresh}
              bene_type={bene_type}
              layout={define}
              step={step}
              amount_component={amountComponent}
              amount_component_props={{amount_data: props.data.amount_indemnify}}
              otpConfig={otpConfig}
              onTabSelect={onTabSelect}
              onNextClick={onNextClick}
              onPreviousClick={onPreviousClick}
              onSaveClick1={onSaveClick1}
              onSaveClick2={onSaveClick2}
              onRejectClick1={onCancelClick1}
              onRejectClick2={onCancelClick2}
              onRejectClick={onRejectClick}
              onBeneChange={onBeneChange}
              onEditBeneClick={onEditBeneClick}
              onEditPaymentClick={onEditPaymentClick}
              onConfirmClick={onConfirmClick}
              onCloseClick={props.onCloseClick}
              onFileUpdate={onFileUpdate}
            />
            </LoadingComponent>


            </div>)}
    </div>
  );
})


export default Modal1;
